import React, {Component} from 'react';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';
import './App.scss';

class App extends Component {

  state = {
    button1: {
      id: 1,
      text: 'Open first modal',
      backgroundColor: 'pink',
    },

    button2: {
      id: 2,
      text: 'Open second modal',
      backgroundColor: 'green',
    },

    modal1: {
      closeButton: true,
      header: 'Do you want to delete this file?',
      text: 'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?',
      actions: (
        <div className="action-buttons">
          <button className="action-button" onClick={this.closeFirstModal.bind(this)}>Ok</button>
          <button className="action-button" onClick={this.closeFirstModal.bind(this)}>Cancel</button>
        </div>
      ),
      isOpen: false
    },

    modal2: {
      closeButton: true,
      header: 'Do you want to close this window?',
      text: 'If you close this window, all your information will be deleted',
      actions: (
        <div className="action-buttons">
          <button className="action-button" onClick={this.closeSecondModal.bind(this)}>Close</button>
          <button className="action-button" onClick={this.closeSecondModal.bind(this)}>Cancel</button>
        </div>
      ),
      isOpen: false
    }
  };

  openModal(id) {
    if (id === 1) {
      this.setState({
        modal1: {
          ...this.state.modal1,
          isOpen: true
        }
      })
    } else {
      this.setState({
        modal2: {
          ...this.state.modal2,
          isOpen: true
        }
      })
    }
  }

  closeFirstModal() {
    this.setState({
      modal1: {
        ...this.state.modal1,
        isOpen: false
      }
    });
  }

  closeSecondModal() {
    this.setState({
      modal2: {
        ...this.state.modal2,
        isOpen: false
      }
    });
  }

  render() {
    const {button1, button2, modal1, modal2} = this.state;

    return (
      <div className="container">
        {modal1.isOpen && <Modal info={modal1} close={this.closeFirstModal.bind(this)}/>}
        {modal2.isOpen && <Modal info={modal2} close={this.closeSecondModal.bind(this)}/>}
        <Button info={button1} handler={this.openModal.bind(this, button1.id)}/>
        <Button info={button2} handler={this.openModal.bind(this, button2.id)}/>
      </div>
    )
  }
}


export default App;