import React, {Component} from "react";
import PropTypes from 'prop-types';
import './Button.scss';

class Button extends Component {

  render() {
    const {info, handler} = this.props;
    return (
      <>
        <button onClick={handler} className="open-modal"
                style={{backgroundColor: info.backgroundColor}}>{info.text}</button>
      </>
    )
  }
}

Button.propTypes = {
  info: PropTypes.shape(
    {
      text: PropTypes.string,
      backgroundColor: PropTypes.string
    }
  ),
  handler: PropTypes.func.isRequired
};

export default Button;