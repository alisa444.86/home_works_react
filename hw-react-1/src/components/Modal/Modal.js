import React, {Component} from 'react';
import './Modal.scss';
import PropTypes from "prop-types";

class Modal extends Component {

  render() {
    const {info, close} = this.props;

    const listener = (event) => {
      if (event.target.classList.contains('modal')) {
        close()
      }
    };

    return (
      <div className="modal" onClick={listener}>
        <div className="modal-body">
          <div className="modal-header">
            <p className="header-text">{info.header}</p>
            {info.closeButton ? <span className="close-button" onClick={close}>X</span> : null}
          </div>
          <p className="modal-text">{info.text}</p>
          {info.actions}
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  info: PropTypes.shape(
    {
      closeButton: PropTypes.bool,
      header: PropTypes.string,
      text: PropTypes.string,
      actions: PropTypes.object,
    }
  ),
  close: PropTypes.func.isRequired
};

export default Modal;