import React, {Component, Fragment} from 'react';
import Modal from './components/Modal/Modal';
import './App.scss';
import axios from 'axios';
import Cards from './containers/Cards/Cards';
import Header from "./components/Header/Header";
import Footer from './components/Footer/Footer';
import Top from './components/Top/Top';

class App extends Component {

  state = {
    goodsInfo: [],
    isModalOpen: false,
    currentVendorCode: 0,
  };

  componentDidMount() {
    axios('/goods.json')
      .then(result => {
        console.log(result.data);
        this.setState({goodsInfo: result.data})
      })
  }

  openModal(articule) {
    this.setState({isModalOpen: true, currentVendorCode: articule})
  }

  closeModal() {
    this.setState({isModalOpen: false})
  }

  addToCart() {
    let goodsInfo = this.state.goodsInfo;

    let itemToCart = goodsInfo.find(item => {
      if (item.vendorCode === this.state.currentVendorCode) {
        return item
      }
    });

    let goodsInCart = JSON.parse(localStorage.getItem('cartItems'));
    goodsInCart = (goodsInCart && goodsInCart.length) ? goodsInCart : [];

    let itemAlreadyInCart = goodsInCart.find(item => {
      if (item.vendorCode === itemToCart.vendorCode) {
        return item
      }
    });

    if (itemAlreadyInCart) {
      itemAlreadyInCart.inCart += 1;
      //to make info in goodsInfo actual
      itemToCart.inCart += 1;
    } else {
      itemToCart.inCart += 1;
      goodsInCart.push(itemToCart);
    }

    localStorage.setItem('cartItems', JSON.stringify(goodsInCart));
  }

  addToFavorite(vendorCode) {
    let goodsInfo = this.state.goodsInfo;

    let favoriteItem = goodsInfo.find(item => {
      if (item.vendorCode === vendorCode) {
        return item
      }
    });

    let favoriteGoods = JSON.parse(localStorage.getItem('favorites'));
    favoriteGoods = (favoriteGoods && favoriteGoods.length) ? favoriteGoods : [];

    let itemInFavorites = favoriteGoods.find(item => {
      if (item.vendorCode === favoriteItem.vendorCode) {
        return item
      }
    });

    if (!itemInFavorites) {
      favoriteItem.isFavorite = true;
      favoriteGoods.push(favoriteItem);
    }
    localStorage.setItem('favorites', JSON.stringify(favoriteGoods));
  }

  render() {
    const {isModalOpen, goodsInfo} = this.state;

    return (
      <Fragment>
        {isModalOpen &&
        <Modal isOpen={isModalOpen} close={this.closeModal.bind(this)} addToCart={this.addToCart.bind(this)}/>}
        <Header/>
        <Top/>
        <Cards cardsData={goodsInfo} handlerModal={this.openModal.bind(this)}
               addFavorite={this.addToFavorite.bind(this)}/>
        <Footer/>
      </Fragment>
    )
  }
}

export default App;