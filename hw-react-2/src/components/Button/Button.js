import React, {Component} from "react";
import PropTypes from 'prop-types';
import './Button.scss';

class Button extends Component {
    render() {
      const {handler, text, classList, vendorCode} = this.props;

      return (
        <button className={classList} onClick={()=> {handler(vendorCode)}}>{text}</button>
      );
    }
}

Button.propTypes = {
  handler: PropTypes.func,
  text: PropTypes.string,
  classList: PropTypes.string,
  vendorCode: PropTypes.number
};

export default Button;