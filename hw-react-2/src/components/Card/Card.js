import React, {Component} from 'react';
import './Card.scss';
import Button from "../Button/Button";
import Star from "../Star/Star";
import PropTypes from 'prop-types';

class Card extends Component {
  state = {
    isFavorite: false,
  };

  handler () {
    this.setState({isFavorite: true})
  }

  render() {
    const {card, handlerModal, addFavorite} = this.props;

    return (
      <div className="card-item">
        <Star card={card} addFavorite={addFavorite} isFavorite={this.state.isFavorite} handler={this.handler.bind(this)}/>
        <img className="card-img-top" src={card.imgSrc} alt="Chair"/>
        <Button handler={handlerModal} vendorCode={card.vendorCode}
                classList={"btn-add-to-cart"} text={"Add To Cart"}/>
        <div className="card-body">
          <p className="card-title">{card.title}</p>
          <div className="footer__price">${card.price}</div>
        </div>
      </div>
    )
  }
}

Card.propTypes ={
  card: PropTypes.object,
  addFavorite: PropTypes.func,
  handlerModal: PropTypes.func
};

export default Card;