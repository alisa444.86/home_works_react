import React, {Component} from 'react';
import './Footer.scss';

class Footer extends Component {
  render() {
    return (
      <footer className="page-footer">
        <div className="footer-container">
          <div className="footer__column">
            <div className="column__title">INFORMATION</div>
            <ul className="column__list">
              <li className="list__item"><a href="#">About us</a></li>
              <li className="list__item"><a href="#">Privacy</a></li>
              <li className="list__item"><a href="#">Conditions</a></li>
              <li className="list__item"><a href="#">Online support</a></li>
            </ul>
          </div>
          <div className="footer__column">
            <div className="column__title">MY ACCOUNT</div>
            <ul className="column__list">
              <li className="list__item"><a href="#">Login</a></li>
              <li className="list__item"><a href="#">My Cart</a></li>
              <li className="list__item"><a href="#">Wishlist</a></li>
              <li className="list__item"><a href="#">Checkout</a></li>
            </ul>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;