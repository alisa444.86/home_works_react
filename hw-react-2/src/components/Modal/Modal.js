import React, {Component} from 'react';
import './Modal.scss';
import PropTypes from "prop-types";

class Modal extends Component {

  state = {
    closeButton: true,
    header: 'Do you want to add this chair to cart?',
    text: 'You can delete it from cart later',
  };

  render() {

    const {closeButton, header, text} = this.state;
    const {isOpen, close, addToCart} = this.props;

    const listener = (event) => {
      if (event.target.classList.contains('modal')) {
        close()
      }
    };

    const add = () => {
      close();
      addToCart();
    };

    return (
      isOpen &&
      <div className="modal" onClick={listener}>
        <div className="modal-body">
          <div className="modal-header">
            <p className="header-text">{header}</p>
            {closeButton ? <span className="close-button" onClick={close}>X</span> : null}
          </div>
          <p className="modal-text">{text}</p>
          <div className="action-buttons">
            <button className="action-button" onClick={add}>Ok</button>
            <button className="action-button" onClick={close}>Cancel</button>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  isOpen: PropTypes.bool,
  close: PropTypes.func,
  addToCart: PropTypes.func
};

export default Modal;