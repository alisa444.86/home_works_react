import React, {Component} from 'react';
import './Star.scss';
import PropTypes from 'prop-types';

class Star extends Component {
  render() {
    const {card, addFavorite, isFavorite, handler} = this.props;
    return (
      <div className="star">
          <span className="fa fa-star" onClick={()=> {handler(); addFavorite(card.vendorCode)}} style={isFavorite ? {color: "#d58e32"} : {color: "slategrey"}}> </span>
      </div>
    );
  }
}

Star.propTypes = {
  card: PropTypes.object,
  addFavorite: PropTypes.func,
  isFavorite: PropTypes.bool,
  handler: PropTypes.func
};

export default Star;