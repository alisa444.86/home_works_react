import React, {Component} from 'react';
import './Top.scss';

class Top extends Component {
  render() {
    return (
      <div className="top-section">
        <div className="inner-top">
          indoor furniture
        </div>
      </div>
    );
  }
}

export default Top;