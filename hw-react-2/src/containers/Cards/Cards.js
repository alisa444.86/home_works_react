import React, {Component} from 'react';
import Card from '../../components/Card/Card';
import './Cards.scss';
import PropTypes from 'prop-types';

class Cards extends Component {
  render() {

    const {cardsData, handlerModal, addFavorite} = this.props;
    const cards = cardsData.map(card => <Card card={card} key={card.vendorCode} handlerModal={handlerModal} addFavorite={addFavorite}/>);
    return (
      <div className="cards-container">
        {cards}
      </div>
    );
  }
}

Cards.propTypes = {
  cardsData: PropTypes.array,
  handlerModal: PropTypes.func,
  addFavorite: PropTypes.func
};

export default Cards;

