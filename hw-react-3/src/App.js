import React, {useState, useEffect, Fragment} from 'react';
import Modal from './components/Modal/Modal';
import './App.scss';
import axios from 'axios';
import Header from "./components/Header/Header";
import Footer from './components/Footer/Footer';
import AppRoutes from "./routes/AppRoutes";

const getStorageData = () => {
  let goodsInStorage = JSON.parse(localStorage.getItem('goodsInStorage'));
  goodsInStorage = (goodsInStorage && goodsInStorage.length) ? goodsInStorage : [];
  return goodsInStorage;
};


const App = (props) => {
  const [goodsInfo, setGoodsInfo] = useState([]);
  const [currentVendorCode, setCurrentVendorCode] = useState(0);

  const [modalAdd, setModalAdd] = useState({
    title: 'modalAdd',
    closeButton: true,
    header: 'Do you want to add this good to cart?',
    text: 'You can delete it from cart later',
    isModalOpen: false
  });

  const [modalDelete, setModalDelete] = useState({
    title: 'modalDelete',
    closeButton: true,
    header: 'Confirm delete action',
    text: 'Are you sure, that you want to delete this good from cart?',
    isModalOpen: false
  });

  useEffect(() => {
    axios('/goods.json')
      .then(result => {
        const goodsInfo = result.data;
        const goodsInStorage = getStorageData();
        const info = [];

        goodsInfo.forEach(item => {
            const itemInStorage = goodsInStorage && goodsInStorage.find(el => el.vendorCode === item.vendorCode);
            if (itemInStorage) {
              item = itemInStorage;
            }
            info.push(item);
          }
        );
        setGoodsInfo(info);
      })

  });

  const openModalAdd = (articule) => {
    setModalAdd(prevState => ({...prevState, isModalOpen: true}));
    setCurrentVendorCode(articule);
  };

  const openModalDelete = (articule) => {
    setModalDelete(prevState => ({...prevState, isModalOpen: true}));
    setCurrentVendorCode(articule);
  };

  const closeModal = () => {
    setModalDelete(prevState => ({...prevState, isModalOpen: false}));
    setModalAdd(prevState => ({...prevState, isModalOpen: false}));
  };

  return (
    <Fragment>
      {modalAdd.isModalOpen &&
      <Modal isOpen={modalAdd.isModalOpen}
             close={closeModal}
             goodsInfo={goodsInfo}
             getStorageData={getStorageData}
             vendorCode={currentVendorCode}
             modalData={modalAdd}/>}
      {modalDelete.isModalOpen &&
      <Modal isOpen={modalDelete.isModalOpen}
             close={closeModal}
             getStorageData={getStorageData}
             vendorCode={currentVendorCode}
             modalData={modalDelete}/>}
      <Header/>
      <AppRoutes cardsData={goodsInfo}
                 handlerModal={openModalAdd}
                 openModalDelete={openModalDelete}
                 getStorageData={getStorageData}
                 vendorCode={currentVendorCode}
                 goodsInfo={goodsInfo}
      />
      <Footer/>
    </Fragment>
  )
};

export default App;