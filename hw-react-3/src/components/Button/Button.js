import React from "react";
import PropTypes from 'prop-types';
import './Button.scss';

const Button = (props) => {
  const {handler, text, classList, vendorCode} = props;

  return (
    <button className={classList} onClick={() => {
      handler(vendorCode)
    }}>{text}</button>
  );
};

Button.propTypes = {
  handler: PropTypes.func,
  text: PropTypes.string,
  classList: PropTypes.string,
  vendorCode: PropTypes.number
};

export default Button;