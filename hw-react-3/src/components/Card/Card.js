import React from 'react';
import './Card.scss';
import Button from "../Button/Button";
import Star from "../Star/Star";
import PropTypes from 'prop-types';

const Card = (props) => {

  const {card, handlerModal, addFavorite, deleteFavorite} = props;

  return (
    <div className="card-item">
      <Star card={card} addFavorite={addFavorite} isFavorite={card.isFavorite} deleteFavorite={deleteFavorite}/>
      {card.inCart > 0 ? <span className="card_counter">In cart: {card.inCart}</span> : null}
      <img className="card-img-top" src={card.imgSrc} alt="Chair"/>
      <Button handler={handlerModal} vendorCode={card.vendorCode}
              classList={"btn-add-to-cart"} text={"Add To Cart"}/>
      <div className="card-body">
        <p className="card-title">{card.title}</p>
        <div className="footer__price">${card.price}</div>
      </div>
    </div>
  )
};

Card.propTypes = {
  card: PropTypes.object,
  addFavorite: PropTypes.func,
  handlerModal: PropTypes.func
};

export default Card;