import React from 'react';
import Star from "../Star/Star";
import Button from "../Button/Button";
import './CartItem.scss';

const CartItem = (props) => {

  const {card, handlerModal, addFavorite, deleteFavorite, openModalDelete} = props;

  return (
    <>
      <div className="cart-item">

        <img className="card-img-top" src={card.imgSrc} alt="Chair"/>

        <div className="card-body">
          <p className="card-title">{card.title}</p>
          <div className="footer__price">Price: ${card.price}</div>
        </div>
        <div className="action-buttons">
          <Star card={card}
                addFavorite={addFavorite}
                isFavorite={card.isFavorite}
                deleteFavorite={deleteFavorite}
          />
          <div className="card_counter">Amount in cart: {card.inCart}</div>

          <Button handler={handlerModal}
                  vendorCode={card.vendorCode}
                  classList={"btn-add-to-cart"}
                  text={"Add one more"}/>
        </div>
        <div className='cart-item-header'>
          <span className='button-delete fa fa-times' onClick={() => {
            openModalDelete(card.vendorCode)
          }}/>
        </div>
      </div>
    </>
  )
};

export default CartItem;