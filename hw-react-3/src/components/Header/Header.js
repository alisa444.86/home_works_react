import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.scss';

const Header = (props) => {

  return (
    <header className="page-header">
      <img src="./bazar-logo.png" alt="logo" className="logo"/>

      <div className="header_links">
        <NavLink exact to="/"
                 className="header_link"
                 activeClassName="header_link--active">
          Home
        </NavLink>
        <NavLink exact to='/cart'
                 className="header_link"
                 activeClassName="header_link--active">
          Cart
        </NavLink>
        <NavLink exact to='/favorites'
                 className='header_link'
                 activeClassName='header_link--active'>
          Favorites
        </NavLink>
      </div>
    </header>
  );
}

export default Header;