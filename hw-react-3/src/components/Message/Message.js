import React from 'react';
import './Message.scss';

const Message = (props) => {
    const {text} = props;
    return (
      <p className='no-items'>{text}</p>
    );
};

export default Message;