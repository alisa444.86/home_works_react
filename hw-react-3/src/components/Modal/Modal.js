import React from 'react';
import './Modal.scss';
import PropTypes from "prop-types";

const Modal = (props) => {

  const {isOpen, close, modalData, goodsInfo, getStorageData, vendorCode} = props;

  const listener = (event) => {
    if (event.target.classList.contains('modal')) {
      close()
    }
  };

  const addToCart = () => {
    let goodsInStorage = getStorageData();
    let itemToAdd = goodsInfo.find(item => item.vendorCode === vendorCode);
    let itemAlreadyInCart = goodsInStorage.find(item => item.vendorCode === itemToAdd.vendorCode);

    itemToAdd.inCart += 1;

    if (itemAlreadyInCart) {
      itemAlreadyInCart.inCart += 1;
    } else {
      goodsInStorage.push(itemToAdd);
    }

    localStorage.setItem('goodsInStorage', JSON.stringify(goodsInStorage));
  };

  const deleteFromCart = () => {
    let goodsInStorage = getStorageData();
    const goodsToStorage = [];

    goodsInStorage.forEach(item => {
        if (item.vendorCode === vendorCode) {
          item.inCart = 0;
        }
      }
    );
    goodsInStorage.forEach(item => {
        if (item.inCart > 0 || item.isFavorite) {
          goodsToStorage.push(item)
        }
      }
    );
    localStorage.setItem('goodsInStorage', JSON.stringify(goodsToStorage));
  };

  return (
    isOpen &&
    <div className="modal" onClick={listener}>
      <div className="modal-body">
        <div className="modal-header">
          <p className="header-text">{modalData.header}</p>
          {modalData.closeButton ? <span className="close-button" onClick={close}>X</span> : null}
        </div>
        <p className="modal-text">{modalData.text}</p>
        <div className="action-buttons">
          <button className="action-button" onClick={() => {
            if (modalData.title === 'modalAdd') {
              addToCart();
            } else {
              deleteFromCart();
            }
            close();
          }}>Ok
          </button>
          <button className="action-button" onClick={close}>Cancel</button>
        </div>
      </div>
    </div>
  )
};

Modal.propTypes = {
  isOpen: PropTypes.bool,
  close: PropTypes.func,
  addToCart: PropTypes.func
};

export default Modal;