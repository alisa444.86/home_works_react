import React from 'react';
import './Star.scss';
import PropTypes from 'prop-types';

const Star = (props) => {

    const {card, addFavorite, deleteFavorite} = props;

    const handler = (card) => {
      if (card.isFavorite) {
        deleteFavorite(card.vendorCode)
      } else {
        addFavorite(card.vendorCode)
      }
    };
    return (
      <div className="star">
          <span className="fa fa-star" onClick={() => handler(card)} style={card.isFavorite ? {color: "#d58e32"} : {color: "slategrey"}}> </span>
      </div>
    )
};

Star.propTypes = {
  card: PropTypes.object,
  addFavorite: PropTypes.func,
  isFavorite: PropTypes.bool,
  handler: PropTypes.func
};

export default Star;