import React from 'react';
import './Top.scss';

const Top = (props) => {
  return (
    <div className="top-section">
      <div className="inner-top">
        indoor furniture
      </div>
    </div>
  )
};

export default Top;