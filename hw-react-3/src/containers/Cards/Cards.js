import React from 'react';
import './Cards.scss';
import PropTypes from 'prop-types';

function Cards(props) {
  const {cardsData, handlerModal, addFavorite, CardComponent,
    deleteFromCart, deleteFavorite, openModalDelete} = props;

  const cards = cardsData.map(card => <CardComponent card={card}
                                                     key={card.vendorCode}
                                                     handlerModal={handlerModal}
                                                     deleteFromCart={deleteFromCart}
                                                     addFavorite={addFavorite}
                                                     deleteFavorite={deleteFavorite}
                                                     openModalDelete={openModalDelete}/>);
  return (
    <div className="cards-container">
      {cards}
    </div>
  );
}

Cards.propTypes = {
  cardsData: PropTypes.array,
  handlerModal: PropTypes.func,
  addFavorite: PropTypes.func
};

export default Cards;

