import React from 'react';
import Cards from "../../containers/Cards/Cards";
import './Cart.scss';
import Message from "../../components/Message/Message";
import CartItem from "../../components/CartItem/CartItem";

const Cart = (props) => {

    const {handlerModal, addFavorite, searchItems, deleteFromCart, deleteFavorite, openModalDelete} = props;
    const cardsData = searchItems('inCart');

    return (
      <div className="cart">
       {cardsData.length > 0 ? <Cards cardsData={cardsData}
                                      deleteFromCart={deleteFromCart}
                                      CardComponent={CartItem}
                                      handlerModal={handlerModal}
                                      addFavorite={addFavorite}
                                      deleteFavorite={deleteFavorite}
                                      openModalDelete={openModalDelete}/>
       : <Message text='No items in your cart'/> }
      </div>
    )
};

export default Cart;