import React from 'react';
import Cards from "../../containers/Cards/Cards";
import './Favorites.scss';
import Message from "../../components/Message/Message";
import Card from "../../components/Card/Card";

const Favorites = (props) => {

  const {handlerModal, searchItems, addFavorite, deleteFavorite} = props;
  const cardsData = searchItems('isFavorite');

  return (
    <div>
      {cardsData.length > 0 ? <Cards CardComponent={Card}
                                     cardsData={cardsData}
                                     handlerModal={handlerModal}
                                     addFavorite={addFavorite}
                                     deleteFavorite={deleteFavorite}/>
        : <Message text='No favorite items'/>}
    </div>
  )
};

export default Favorites;