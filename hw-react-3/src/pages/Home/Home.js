import React from 'react';
import Top from "../../components/Top/Top";
import Cards from "../../containers/Cards/Cards";
import Card from '../../components/Card/Card';

const Home = (props) => {

    const {cardsData, handlerModal, addFavorite, deleteFavorite} = props;

    return (
      <div>
        <Top />
        <Cards cardsData={cardsData} handlerModal={handlerModal}
               addFavorite={addFavorite} CardComponent={Card} deleteFavorite={deleteFavorite}/>
      </div>
    )
};

export default Home;