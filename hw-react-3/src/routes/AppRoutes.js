import React, {useState} from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from "../pages/Home/Home";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";

function AppRoutes(props) {
  const [favoritesCounter, setFavoritesCounter] = useState(0);
  const {cardsData, handlerModal, openModalDelete, getStorageData, goodsInfo} = props;

  const searchItems = (property) => {
    let goodsInStorage = getStorageData();
    const cardsData = [];

    if (goodsInStorage) {
      goodsInStorage.forEach(item => {
        if (item[property]) {
          cardsData.push(item)
        }
      });
    }
    return cardsData
  };

  const addFavorite = (vendorCode) => {
    let goodsInStorage = getStorageData();
    let favoriteItem = goodsInfo.find(item => item.vendorCode === vendorCode);
    let itemInStorage = goodsInStorage.find(item => item.vendorCode === favoriteItem.vendorCode);

    if (itemInStorage && !itemInStorage.isFavorite) {
      itemInStorage.isFavorite = true
    }

    favoriteItem.isFavorite = true;

    if (!itemInStorage) {
      goodsInStorage.push(favoriteItem);
    }

    setFavoritesCounter(favoritesCounter + 1);

    localStorage.setItem('goodsInStorage', JSON.stringify(goodsInStorage));
  };

  const deleteFavorite = (vendorCode) => {
    let goodsInStorage = getStorageData();
    const goodsToStorage = [];

    goodsInStorage.forEach(item => {
        if (item.vendorCode === vendorCode) {
          item.isFavorite = false;
        }
      }
    );

    goodsInStorage.forEach(item => {
      if (item.isFavorite || item.inCart > 0) {
        goodsToStorage.push(item)
      }
    });
    localStorage.setItem('goodsInStorage', JSON.stringify(goodsToStorage));
  };

  return (
    <div className='page-wrapper'>
      <Switch>
        <Route exact path='/' render={(props) =>
          <Home cardsData={cardsData}
                handlerModal={handlerModal}
                addFavorite={addFavorite}
                deleteFavorite={deleteFavorite}
                {...props} />
        }/>
        <Route exact path='/cart' render={props =>
          <Cart cardsData={cardsData}
                handlerModal={handlerModal}
                addFavorite={addFavorite}
                searchItems={searchItems}
                deleteFavorite={deleteFavorite}
                openModalDelete={openModalDelete}
                {...props} />
        }/>
        <Route exact path='/favorites' render={props =>
          <Favorites cardsData={cardsData}
                     handlerModal={handlerModal}
                     addFavorite={addFavorite}
                     searchItems={searchItems}
                     deleteFavorite={deleteFavorite}
                     {...props} />
        }/>
      </Switch>
    </div>
  );
}

export default AppRoutes;