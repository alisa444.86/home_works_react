import React, {useEffect} from 'react';
import ModalAdd from './components/ModalAdd/ModalAdd';
import ModalDelete from "./components/ModalDelete/ModalDelete";
import './App.scss';
import {connect} from 'react-redux';

import Header from "./components/Header/Header";
import Footer from './components/Footer/Footer';
import AppRoutes from "./routes/AppRoutes";
import {getGoodsInfo} from "./store/actionCreators/actions";

const App = (props) => {
  const {getGoodsInfo, modalAddOpen, modalDeleteOpen} = props;

  useEffect(() => {
    getGoodsInfo();
  }, []);

  return (
    <div>
      {modalAddOpen && <ModalAdd/>}
      {modalDeleteOpen && <ModalDelete/>}
      <Header/>
      <AppRoutes/>
      <Footer/>
    </div>
  )
};

const mapStateToProps = (store) => {
  return {
    goodsInfo: store.goodsInfo,
    modalAddOpen: store.modalAddOpen,
    modalDeleteOpen: store.modalDeleteOpen
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getGoodsInfo: (goodsInfo) => {
      dispatch(getGoodsInfo(goodsInfo))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
