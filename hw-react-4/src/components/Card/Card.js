import React from 'react';
import PropTypes from 'prop-types';

import Button from "../Button/Button";
import Star from "../Star/Star";
import './Card.scss';

const Card = (props) => {
  const {card} = props;
  const {inCart, vendorCode, title, price, imgSrc} = card;

  return (
    <div className="card-item">
      <Star card={card}/>
      {inCart > 0 ? <span className="card_counter">In cart: {inCart}</span> : null}
      <img className="card-img-top" src={imgSrc} alt="Chair"/>
      <Button vendorCode={vendorCode}
              classList={"btn-add-to-cart"}
              text={"Add To Cart"}/>
      <div className="card-body">
        <p className="card-title">{title}</p>
        <div className="footer__price">${price}</div>
      </div>
    </div>
  )
};

Card.propTypes = {
  card: PropTypes.object,
  addFavorite: PropTypes.func,
  handlerModal: PropTypes.func
};

export default Card;