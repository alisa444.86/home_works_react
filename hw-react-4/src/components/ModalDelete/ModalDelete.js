import React from 'react';
import {connect} from 'react-redux';

import './ModalDelete.scss';
import {setStorageData} from "../../actions/storageActions";
import {setModalDeleteOpen, deleteFromCart} from "../../store/actionCreators/actions";

const ModalDelete = (props) => {

  const {vendorCode, setModalDeleteOpen, deleteFromCart, goodsInfo} = props;

  const close = () => {
    setModalDeleteOpen(false);
  };

  const listener = (event) => {
    if (event.target.classList.contains('modal')) {
      close()
    }
  };

  const deleteCard = () => {
    let itemToDelete = goodsInfo.find(item => item.vendorCode === vendorCode);
    deleteFromCart(itemToDelete);
    close();
  };

  setStorageData(goodsInfo);

  return (
    <div className="modal" onClick={listener}>
      <div className="modal-body">
        <div className="modal-header">
          <p className="header-text">Confirm delete action</p>
          <span className="close-button fa fa-times" onClick={close}/>
        </div>
        <p className="modal-text">Are you sure, that you want to delete this good from cart?</p>
        <div className="action-buttons">
          <button className="action-button" onClick={deleteCard}>Ok</button>
          <button className="action-button" onClick={close}>Cancel</button>
        </div>
      </div>
    </div>
  )
};

const mapStateToProps = (store) => {
  return {
    goodsInfo: store.goodsInfo,
    vendorCode: store.currentVendorCode
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setModalDeleteOpen: modalDelete => dispatch(setModalDeleteOpen(modalDelete)),
    deleteFromCart: card => dispatch(deleteFromCart(card))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalDelete);