import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import './Star.scss';
import {setStorageData} from "../../actions/storageActions";
import {toggleFavorite} from '../../store/actionCreators/actions';

const Star = (props) => {

  const {card, goodsInfo, toggleFavorite} = props;
  const {isFavorite} = card;

  const handlerFavorite = (card) => {
    toggleFavorite(card);
  };

  setStorageData(goodsInfo);

  return (
    <div className="star">
      <span className="fa fa-star" onClick={() => handlerFavorite(card)}
            style={isFavorite ? {color: "#d58e32"} : {color: "slategrey"}}> </span>
    </div>
  )
};

Star.propTypes = {
  card: PropTypes.object,
  goodsInfo: PropTypes.array,
  toggleFavorite: PropTypes.func
};

const mapStateToProps = (store) => {
  return {goodsInfo: store.goodsInfo}
};

const mapDispatchToProps = dispatch => {
  return {toggleFavorite: item => dispatch(toggleFavorite(item))}
};

export default connect(mapStateToProps, mapDispatchToProps)(Star);