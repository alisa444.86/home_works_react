import React from 'react';
import {connect} from 'react-redux';

import './Cart.scss';
import Cards from "../../containers/Cards/Cards";
import EmptyState from "../../components/EmptyState/EmptyState";
import CartItem from "../../components/CartItem/CartItem";

const Cart = (props) => {

  const {cardsData} = props;

  return (
    <div className="cart">
      {cardsData.length > 0 ? <Cards cardsData={cardsData}
                                     CardComponent={CartItem}
        />
        : <EmptyState text='No items in your cart'/>}
    </div>
  )
};

const mapStateToProps = store => {
  return {cardsData: store.goodsInfo.filter(item => item.inCart)}
};

export default connect(mapStateToProps)(Cart);