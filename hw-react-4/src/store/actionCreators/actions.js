import Actions from "../constants/constants";
import axios from "axios";
import {getStorageData} from "../../actions/storageActions";

export const setCurrentVendorCode = (vendorCode) => dispatch => {
  dispatch({type: Actions.SET_CURRENT_VENDORCODE, payload: vendorCode})
};
export const setModalAddOpen = (data) => dispatch => {
  dispatch({type: Actions.SET_MODAL_ADD_OPEN, payload: data})
};
export const setModalDeleteOpen = (data) => dispatch => {
  dispatch({type: Actions.SET_MODAL_DELETE_OPEN, payload: data})
};
export const increaseInCart = (item) => dispatch => {
  dispatch({type: Actions.INCREASE_AMOUNT_IN_CART, payload: item})
};
export const decreaseInCart = (item) => dispatch => {
  dispatch({type: Actions.DECREASE_AMOUNT_IN_CART, payload: item})
};
export const deleteFromCart = (item) => dispatch => {
  dispatch({type: Actions.DELETE_FROM_CART, payload: item})
};
export const toggleFavorite = (item) => dispatch => {
  dispatch({type: Actions.TOGGLE_FAVORITE, payload: item})
};
export const getGoodsInfo = () => dispatch => {
  axios('/goods.json')
    .then(result => {
      const goodsInfo = result.data;
      const goodsInStorage = getStorageData();
      const info = [];

      goodsInfo.forEach(item => {
          const itemInStorage = goodsInStorage && goodsInStorage.find(el => el.vendorCode === item.vendorCode);
          if (itemInStorage) {
            item = itemInStorage;
          }
          info.push(item);
        }
      );
      dispatch({type: Actions.SET_GOODS_INFO, payload: info});
    })
};