const Actions = {
  SET_GOODS_INFO: 'SET_GOODS_INFO',
  SET_CURRENT_VENDORCODE: 'SET_CURRENT_VENDORCODE',
  SET_MODAL_ADD_OPEN: 'SET_MODAL_ADD_OPEN',
  SET_MODAL_DELETE_OPEN: 'SET_MODAL_DELETE_OPEN',
  INCREASE_AMOUNT_IN_CART: 'INCREASE_AMOUNT_IN_CART',
  DECREASE_AMOUNT_IN_CART: 'DECREASE_AMOUNT_IN_CART',
  DELETE_FROM_CART: 'DELETE_FROM_CART',
  TOGGLE_FAVORITE: 'TOGGLE_FAVORITE'
};

export default Actions;