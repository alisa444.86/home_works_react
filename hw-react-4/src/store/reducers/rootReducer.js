import {combineReducers} from "redux";
import modalAddOpen from "./modalAddReducer";
import modalDeleteOpen from './modalDeleteReducer';
import goodsInfo from './goodsInfoReducer';
import currentVendorCode from "./vendorCodeReducer";

export const rootReducer = combineReducers({
  modalAddOpen, currentVendorCode, modalDeleteOpen, goodsInfo
});
