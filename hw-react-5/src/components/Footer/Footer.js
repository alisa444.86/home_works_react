import React from 'react';
import {Link} from 'react-router-dom';
import './Footer.scss';

const Footer = (props) => {

  return (
    <footer className="page-footer">
      <div className="footer-container">
        <div className="footer__column">
          <div className="column__title">INFORMATION</div>
          <ul className="column__list">
            <li className="list__item">
              <button className='footer__link-button'>About us</button>
            </li>
            <li className="list__item">
              <button className='footer__link-button'>Privacy</button>
            </li>
            <li className="list__item">
              <button className='footer__link-button'>Conditions</button>
            </li>
            <li className="list__item">
              <button className='footer__link-button'>Online support</button>
            </li>
          </ul>
        </div>
        <div className="footer__column">
          <div className="column__title">MY ACCOUNT</div>
          <ul className="column__list">
            <li className="list__item">
              <button className='footer__link-button'>Login</button>
            </li>
            <li className="list__item"><Link to='/cart'>Cart</Link></li>
            <li className="list__item"><Link to='/favorites'>Favorites</Link></li>
            <li className="list__item">
              <button className='footer__link-button'>Checkout</button>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
};

export default Footer;