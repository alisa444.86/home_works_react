import * as yup from 'yup';

export const formSchema = yup.object().shape({
  name: yup
    .string()
    .required('This field is required')
    .min(2, 'Minimum name length is 2 symbols'),
  lastName: yup
    .string()
    .required('This field is required')
    .min(2, 'Minimum last name length is 2 symbols'),
  age: yup
    .number('Age must be an integer')
    .integer('Age must be an integer')
    .positive('Age must be a positive integer')
    .moreThan(15, 'You are underage')
    .required('This field is required'),
  email: yup
    .string()
    .required('This field is required')
    .email('Please enter a valid email'),
  address: yup
    .string()
    .required('This field is required'),
  phone: yup
    .string()
    .min(10, 'Minimum phone number length is 10 symbols')
    .required('This field is required'),
});
