import React from 'react';
import PropTypes from 'prop-types';
import './Cards.scss';

function Cards(props) {
  const {cardsData, CardComponent} = props;

  const cards = cardsData.map(card => <CardComponent card={card}
                                                     key={card.vendorCode}
  />);
  return (
    <div className="cards-container">
      {cards}
    </div>
  );
}

Cards.propTypes = {
  cardsData: PropTypes.array,
  addFavorite: PropTypes.func
};

export default Cards;

