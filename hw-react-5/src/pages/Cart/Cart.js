import React, {useState} from 'react';
import {connect} from 'react-redux';

import './Cart.scss';
import Cards from "../../containers/Cards/Cards";
import EmptyState from "../../components/EmptyState/EmptyState";
import CartItem from "../../components/CartItem/CartItem";
import FormikForm from "../../components/Form/Form";
import {setOpenForm} from '../../store/actionCreators/actions'

const Cart = (props) => {

  const {cardsData, openForm, setOpenForm} = props;
  return (
    <div className='cart-page'>
      {openForm && <FormikForm />}
      <div className="cart">
        {cardsData.length > 0 &&
          <div className="wrapper">
            <Cards
              cardsData={cardsData}
              CardComponent={CartItem}/>
            <button className="buy-button" onClick={()=> setOpenForm(!openForm)}>Buy now</button>
          </div>}
        {cardsData.length === 0 && <EmptyState text='No items in your cart'/>}
      </div>
    </div>
  )
};

const mapStateToProps = store => {
  return {
    cardsData: store.goodsInfo.filter(item => item.inCart),
    openForm: store.openForm
  }
};

const mapDispatchToProps = dispatch => {
  return {setOpenForm: bool => dispatch(setOpenForm(bool))}
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);