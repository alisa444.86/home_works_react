import Actions from '../constants/constants';

const formData = (state = {}, action) => {
  switch (action.type) {
    case Actions.SUBMIT_FORM:
      return action.payload;
  }
  return state
};

export default formData;