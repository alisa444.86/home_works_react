import Actions from "../constants/constants";

const LogDeliveryData = (state = null, action) => {
  switch (action.type) {
    case Actions.LOG_DELIVERY_DATA:
      console.log(action.payload);
      return state;
    case Actions.LOG_CHECKOUT_DATA:
      console.log(`You have bought: ${action.payload}`);
      return state;
  }
  return state
};

export default LogDeliveryData;