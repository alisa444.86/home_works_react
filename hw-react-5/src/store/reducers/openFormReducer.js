import Actions from '../constants/constants';

const OpenForm = (state = false, action) => {
  switch (action.type) {
    case Actions.OPEN_FORM:
      return action.payload
  }
  return state
};

export default OpenForm;