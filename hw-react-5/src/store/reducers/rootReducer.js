import {combineReducers} from "redux";
import modalAddOpen from "./modalAddReducer";
import modalDeleteOpen from './modalDeleteReducer';
import goodsInfo from './goodsInfoReducer';
import openForm from './openFormReducer';
import currentVendorCode from "./vendorCodeReducer";
import formData from './formSubmitReducer';
import logDeliveryData from './logDataReducer';

export const rootReducer = combineReducers({
  modalAddOpen, currentVendorCode, modalDeleteOpen, goodsInfo, openForm, formData, logDeliveryData
});
