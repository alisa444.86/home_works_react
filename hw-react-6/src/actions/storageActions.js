export const getStorageData = () => {
  let goodsInStorage = JSON.parse(localStorage.getItem('goodsInStorage'));
  goodsInStorage = (goodsInStorage && goodsInStorage.length) ? goodsInStorage : [];
  return goodsInStorage;
};

export const setStorageData = (data) => {
  localStorage.setItem('goodsInStorage', JSON.stringify(data));
};
