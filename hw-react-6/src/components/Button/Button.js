import React from "react";
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import './Button.scss';
import {setModalAddOpen, setCurrentVendorCode} from "../../store/actionCreators/actions";

export const Button = (props) => {
  const {text, classList, vendorCode, setModalAddOpen, setCurrentVendorCode} = props;

  return (
    <button data-testid='button-test' className={classList} onClick={() => {
      setModalAddOpen(true);
      setCurrentVendorCode(vendorCode);
    }}>{text}</button>
  );
};

Button.propTypes = {
  handler: PropTypes.func,
  text: PropTypes.string,
  classList: PropTypes.string,
  vendorCode: PropTypes.number
};

const mapDispatchToProps = (dispatch) => {
  return {
    setModalAddOpen: (flag) => dispatch(setModalAddOpen(flag)),
    setCurrentVendorCode: (vendorCode) => dispatch(setCurrentVendorCode(vendorCode))
  }
};

export default connect(null, mapDispatchToProps)(Button);