import React from 'react';
import {Button} from './Button';
import {render} from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

describe('Button testing', () => {

  test('Button is renders properly', () => {
    render(<Button />);
    const button = document.querySelector('button');
    expect(button).toBeInTheDocument();
  });

  test('Button renders text', () => {
    const testText = 'add to cart';
    render(<Button text={testText}/>);
    const button = document.querySelector('button');
    expect(button.textContent).toBe(testText);
  });

  test('Button has passed classList', () => {
    render(<Button classList='testClass'/>);
    const button = document.querySelector('button');
    expect(button).toHaveClass('testClass');
  });

  it('on button click function setModalOpen is called with "true" argument', () => {
    const setModalAddOpenMock = jest.fn();
    const setCurrentVendorCodeMock = jest.fn();
    const testCode = 1;

    const {getByTestId} = render(<Button vendorCode={testCode}
                                         setModalAddOpen={setModalAddOpenMock}
                                         setCurrentVendorCode={setCurrentVendorCodeMock}
                                       />);

    expect(setModalAddOpenMock).not.toHaveBeenCalled();

    getByTestId('button-test').dispatchEvent(new MouseEvent("click", {bubbles: true}));
    expect(setModalAddOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalAddOpenMock).toHaveBeenCalledWith(true);

    expect(setCurrentVendorCodeMock).toHaveBeenCalledTimes(1);
    expect(setCurrentVendorCodeMock).toHaveBeenCalledWith(testCode);
  })

});