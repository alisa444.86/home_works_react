import React from 'react';
import Card from './Card';
import {store} from "../../store/store";
import {Provider} from 'react-redux';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('card testing', () => {
  const testCard = {
    inCart: 1,
    vendorCode: 1,
    title: 'testTitle',
    price: 'testPrice',
    imgSrc: 'testSrc'
  };

  it('card renders with proper values', () => {
    const {container} = render(<Provider store={store}><Card card={testCard}/></Provider>);
    const title = container.querySelector('.card-title');
    expect(title.textContent).toBe(testCard.title);

    const price = container.querySelector('.footer__price');
    expect(price.textContent).toBe(`$${testCard.price}`);
  });

  it('cardCounter shows proper value and does not render when good is not in cart', () => {
    const {container, rerender} = render(<Provider store={store}><Card card={testCard}/></Provider>);
    const cardCounter = container.querySelector('.card_counter');
    expect(cardCounter.textContent).toBe('In cart: 1');

    testCard.inCart = 0;

    rerender(<Provider store={store}><Card card={testCard}/></Provider>);
    expect(cardCounter).not.toBeInTheDocument();
  });
});