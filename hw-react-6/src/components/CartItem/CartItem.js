import React from 'react';
import {connect} from 'react-redux';

import './CartItem.scss';
import Star from "../Star/Star";
import {
  setCurrentVendorCode,
  setModalDeleteOpen,
  increaseInCart,
  decreaseInCart
} from "../../store/actionCreators/actions";
import {setStorageData} from "../../actions/storageActions";

export const CartItem = (props) => {

  const {card, goodsInfo, setCurrentVendorCode, setModalDeleteOpen, decreaseInCart, increaseInCart} = props;
  const {imgSrc, title, price, inCart, vendorCode} = card;

  setStorageData(goodsInfo);

  return (
    <>
      <div className="cart-item">
        <img className="card-img-top" src={imgSrc} alt="Chair"/>
        <div className="card-body">
          <p className="card-title">{title}</p>
          <div className="footer__price">Price: ${price}</div>
        </div>
        <div className="action-buttons">
          <Star card={card}/>
          <div className="card_counter counter-box">
            <span className="counter-title">Amount in cart:</span>
            <div className="counter">
              <span className="minus-btn" onClick={() => {
                decreaseInCart(card);
              }}>-</span>
              {inCart}
              <span className="plus-btn" onClick={() => {
                increaseInCart(card);
              }}>+</span>
            </div>
          </div>
        </div>
        <div className='cart-item-header'>
          <span className='button-delete fa fa-times' onClick={() => {
            setModalDeleteOpen(true);
            setCurrentVendorCode(vendorCode);
          }}/>
        </div>
      </div>
    </>
  )
};

const mapStateToProps = (store) => {
  return {
    goodsInfo: store.goodsInfo,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setModalDeleteOpen: flag => dispatch(setModalDeleteOpen(flag)),
    setCurrentVendorCode: vendorCode => dispatch(setCurrentVendorCode(vendorCode)),
    increaseInCart: item => dispatch(increaseInCart(item)),
    decreaseInCart: item => dispatch(decreaseInCart(item))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(CartItem);