import React from 'react';
import {CartItem} from './CartItem';
import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

jest.mock('../Star/Star');

describe('cart item testing', () => {
  const testCard = {
    imgSrc: 'testSrc',
    title: 'testTitle',
    price: 'testPrice',
    inCart: 1,
    vendorCode: 1,
    isFavorite: false
  };

 it('cart item renders properly', () => {
    const {container} = render(<CartItem card={testCard}/>);

    const title = container.querySelector('.card-title');
    expect(title.textContent).toBe(testCard.title);

    const price = container.querySelector('.footer__price');
    expect(price.textContent).toBe(`Price: $${testCard.price}`);
  });

  it('increase button works properly', () => {
    const increaseInCartMock = jest.fn();
    increaseInCartMock.mockImplementation((card) => {
      card.inCart += 1;
    });

    const {container} = render(<CartItem card={testCard}
                                         increaseInCart={increaseInCartMock}/>);

    const plusBtn = container.querySelector('.plus-btn');
    fireEvent.click(plusBtn);
    expect(increaseInCartMock).toHaveBeenCalledTimes(1);
    expect(testCard.inCart).toEqual(2);
  });

  it('decrease button works properly', () => {
    const decreaseInCartMock = jest.fn();
    decreaseInCartMock.mockImplementation((card) => {
      card.inCart -= 1;
    });

    const {container} = render(<CartItem card={testCard}
                                         decreaseInCart={decreaseInCartMock}
    />);

    const minusBtn = container.querySelector('.minus-btn');
    fireEvent.click(minusBtn);
    expect(decreaseInCartMock).toHaveBeenCalledTimes(1);
    expect(testCard.inCart).toEqual(1);
  });

  it('functions to open modal window and to set vendor code are called', () => {
    const setModalDeleteOpenMock = jest.fn();
    const setCurrentVendorCodeMock = jest.fn();
    const {container} = render(<CartItem card={testCard}
                                         setModalDeleteOpen={setModalDeleteOpenMock}
                                         setCurrentVendorCode={setCurrentVendorCodeMock}
    />);
    const btnDelete = container.querySelector('.button-delete');
    fireEvent.click(btnDelete);
    expect(setModalDeleteOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalDeleteOpenMock).toHaveBeenCalledWith(true);
    expect(setCurrentVendorCodeMock).toHaveBeenCalledTimes(1);
    expect(setCurrentVendorCodeMock).toHaveBeenCalledWith(testCard.vendorCode);
  });
});