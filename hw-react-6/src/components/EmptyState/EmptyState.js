import React from 'react';
import './EmptyState.scss';

const EmptyState = (props) => {
  const {text} = props;
  return (
    <p className='no-items'>{text}</p>
  );
};

export default EmptyState;