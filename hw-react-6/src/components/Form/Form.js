import React from 'react';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import {connect} from 'react-redux';
import {setOpenForm, setFormData, clearStorage, getGoodsInfo, logDeliveryData, logCheckoutData} from '../../store/actionCreators/actions';
import {formSchema} from './FormSceme';
import './Form.scss';

export const FormikForm = (props) => {
  const {setOpenForm, setFormData, clearStorage, cardsData, getGoodsInfo, logDeliveryData, logCheckoutData} = props;

  const handleSubmit = (values, {setSubmitting}) => {
    logDeliveryData(values);
    logCheckoutData(cardsData);
    setFormData(values);
    setSubmitting(false);
    setOpenForm(false);
    getGoodsInfo();
    clearStorage();
  };

  return (
      <Formik initialValues={{
        name: '',
        lastName: '',
        age: '',
        email: '',
        address: '',
        deliveryTime: '9-12',
        phone: ''
      }}
              validationSchema={formSchema}
              onSubmit={handleSubmit}
      >
        {(formikProps) => {

          return (
            <div className="modal-form">
              <div className="modal">
                <div className="modal-body">
                  <div className="modal-header">
                    <div className="close-button"><span className="fa fa-times" onClick={() => {
                      setOpenForm(false)
                    }}/></div>
                    <div className="header-text">Enter info for delivery</div>
                  </div>
                  <Form className='form' data-testid="form">
                    <div>
                      <Field className="form-input" component='input' name='name' type='text' placeholder='Name'/>
                      <div className='error'><ErrorMessage name='name'/></div>
                    </div>
                    <div>
                      <Field className="form-input" component='input' name='lastName' type='text'
                             placeholder='Last name'/>
                      <div className='error'><ErrorMessage name='lastName'/></div>
                    </div>
                    <div>
                      <Field className="form-input" component='input' name='age' type='number' placeholder='Age'/>
                      <div className='error'><ErrorMessage name='age'/></div>
                    </div>
                    <div>
                      <Field className="form-input" component='input' name='email' type='email' placeholder='Email'/>
                      <div className='error'><ErrorMessage name='email'/></div>
                    </div>
                    <div>
                      <Field className="form-input" component='textarea' name='address' type='text'
                             placeholder='Address'/>
                      <div className='error'><ErrorMessage name='address'/></div>
                    </div>
                    <div>
                      <Field className="form-input" component='input' name='phone' type='phone'
                             placeholder='Phone number'/>
                      <div className='error'><ErrorMessage name='phone'/></div>
                    </div>
                    <div className='select-box'>
                      <label htmlFor="select" className='label'>Delivery Time:</label>
                      <Field className="form-select" as='select' name='deliveryTime' id='select'>
                        <option value="9-12">9-12</option>
                        <option value="12-15">12-15</option>
                        <option value="15-18">15-18</option>
                      </Field>
                    </div>
                    <div className="action-buttons">
                      <button className="form-button" type='button' onClick={formikProps.resetForm}>Reset</button>
                      <button className="form-button" type='submit' disabled={!formikProps.isValid}>Checkout</button>
                    </div>
                  </Form>
                </div>
              </div>
            </div>
          )
        }}
      </Formik>
  )
};

const mapStateToProps = store => {
  return {
    cardsData: store.goodsInfo.filter(item => item.inCart),
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setOpenForm: bool => dispatch(setOpenForm(bool)),
    setFormData: data => dispatch(setFormData(data)),
    clearStorage: () => dispatch(clearStorage()),
    getGoodsInfo: info => dispatch(getGoodsInfo(info)),
    logDeliveryData: data => dispatch(logDeliveryData(data)),
    logCheckoutData: data => dispatch(logCheckoutData(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FormikForm);