import React from 'react';
import {connect} from 'react-redux';

import {setStorageData} from "../../actions/storageActions";
import {setModalAddOpen, increaseInCart} from "../../store/actionCreators/actions";

export const ModalAdd = (props) => {

  const {goodsInfo, vendorCode, setModalAddOpen, increaseInCart} = props;

  const close = () => {
    setModalAddOpen(false);
  };

  const listener = (event) => {
    if (event.target.classList.contains('modal')) {
      close()
    }
  };

  const addToCart = () => {
    let itemToAdd = goodsInfo.find(item => item.vendorCode === vendorCode);
    increaseInCart(itemToAdd);
  };

  setStorageData(goodsInfo);

  return (
    <div className="modal" onClick={listener}>
      <div className="modal-body">
        <div className="modal-header">
          <p className="header-text">Do you want to add this good to cart?</p>
          <span className="close-button fa fa-times" onClick={close}/>
        </div>
        <p className="modal-text">You can delete it from cart later</p>
        <div className="action-buttons">
          <button className="action-button" onClick={() => {
            addToCart();
            close()
          }}>Ok
          </button>
          <button className="action-button" onClick={close}>Cancel</button>
        </div>
      </div>
    </div>
  )
};

const mapStateToProps = (store) => {
  return {
    goodsInfo: store.goodsInfo,
    vendorCode: store.currentVendorCode,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setModalAddOpen: modalAdd => dispatch(setModalAddOpen(modalAdd)),
    increaseInCart: card => dispatch(increaseInCart(card))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalAdd);