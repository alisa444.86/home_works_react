import React from 'react';
import {ModalAdd} from './ModalAdd';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('ModalAdd testing', () => {

  it('ModalAdd renders properly', () => {
    const {getByText} = render(<ModalAdd/>);
    expect(getByText(/add/i)).toBeInTheDocument();
  });

  it('header renders proper text', () => {
    render(<ModalAdd/>);
    const header = document.querySelector('.header-text');
    const testText = 'Do you want to add this good to cart?';
    expect(header.textContent).toBe(testText);
  });

  it('close button is rendered', () => {
    render(<ModalAdd/>);
    const closeBtn = document.querySelector('.close-button');
    expect(closeBtn).toBeInTheDocument();
  });

  it('action buttons are rendered', () => {
    render(<ModalAdd/>);
    const actionButtons = document.querySelectorAll('.action-button');
    expect(actionButtons[0]).toBeInTheDocument();
  });

  it('on click to close button function setModallAddOpen is called once with right argument', () => {
    const setModalAddOpenMock = jest.fn();
    render(<ModalAdd setModalAddOpen={setModalAddOpenMock}/>);

    const closeBtn = document.querySelector('.close-button');
    closeBtn.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(setModalAddOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalAddOpenMock).toHaveBeenCalledWith(false);
  });

  it('on click to the OK button functions are called', () => {
    const setModalAddOpenMock = jest.fn();
    const increaseInCartMock = jest.fn();
    const setDataMock = jest.fn();
    const testGoodsInfo = [{
      vendorCode: 1
    }, {
      vendorCode: 2
    }];
    const testVendorCode = 1;

    const {getByText} = render(<ModalAdd increaseInCart={increaseInCartMock}
                                         setModalAddOpen={setModalAddOpenMock}
                                         goodsInfo={testGoodsInfo}
                                         vendorCode={testVendorCode}
                                         />);
    (getByText('Ok')).dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(increaseInCartMock).toHaveBeenCalledTimes(1);
    expect(increaseInCartMock).toHaveBeenCalledWith(testGoodsInfo[0]);

    expect(setModalAddOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalAddOpenMock).toHaveBeenCalledWith(false);
  });

  it('on click to the "Cancel" button function setModalAddOpen is called once with right argument', () => {
    const setModalAddOpenMock = jest.fn();
    const {getByText} = render(<ModalAdd setModalAddOpen={setModalAddOpenMock}/>);
    const cancelBtn = getByText('Cancel');

    cancelBtn.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(setModalAddOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalAddOpenMock).toHaveBeenCalledWith(false);
  });

  it('on click outside of modal window the setModalOpenAdd is called', () => {
    const setModalAddOpenMock = jest.fn();
    render(<ModalAdd setModalAddOpen={setModalAddOpenMock}/>);
    const modal = document.querySelector('.modal');

    modal.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(setModalAddOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalAddOpenMock).toHaveBeenCalledWith(false);
  });
});