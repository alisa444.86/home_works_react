import React from 'react';
import {ModalDelete} from './ModalDelete';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('ModalDelete testing', () => {

  it('ModalDelete renders', () => {
    const {getByText} = render(<ModalDelete/>);
    expect(getByText(/confirm/i)).toBeInTheDocument();
  });

  it('header renders proper text', () => {
    render(<ModalDelete/>);
    const header = document.querySelector('.header-text');
    const testText = 'Confirm delete action';
    expect(header.textContent).toBe(testText);
  });

  it('modal body renders proper text', () => {
    render(<ModalDelete/>);
    const body = document.querySelector('.modal-text');
    const testText = 'Are you sure, that you want to delete this good from cart?';
    expect(body.textContent).toBe(testText);
  });

  it('close button is rendered', () => {
    render(<ModalDelete/>);
    const closeBtn = document.querySelector('.close-button');
    expect(closeBtn).toBeInTheDocument();
  });

  it('action buttons are rendered', () => {
    render(<ModalDelete/>);
    const actionButtons = document.querySelectorAll('.action-button');
    expect(actionButtons[0]).toBeInTheDocument();
    expect(actionButtons[1]).toBeInTheDocument();
  });

  it('on click to close button function setModalDeleteOpen is called once with right argument', () => {
    const setModalDeleteOpenMock = jest.fn();
    render(<ModalDelete setModalDeleteOpen={setModalDeleteOpenMock}/>);

    const closeBtn = document.querySelector('.close-button');
    closeBtn.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(setModalDeleteOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalDeleteOpenMock).toHaveBeenCalledWith(false);
  });

  it('on click to the OK button functions are called', () => {
    const setModalDeleteOpenMock = jest.fn();
    const deleteFromCartMock = jest.fn();
    const testGoodsInfo = [{
      vendorCode: 1
    }, {
      vendorCode: 2
    }];
    const testVendorCode = 1;

    const {getByText} = render(<ModalDelete
                                         setModalDeleteOpen={setModalDeleteOpenMock}
                                         goodsInfo={testGoodsInfo}
                                         vendorCode={testVendorCode}
                                         deleteFromCart={deleteFromCartMock}
                                         />);
    (getByText('Ok')).dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(deleteFromCartMock).toHaveBeenCalledTimes(1);
    expect(deleteFromCartMock).toHaveBeenCalledWith(testGoodsInfo[0]);

    expect(setModalDeleteOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalDeleteOpenMock).toHaveBeenCalledWith(false);
  });

  it('on click to the "Cancel" button function setModalDeleteOpen is called once with right argument', () => {
    const setModalDeleteOpenMock = jest.fn();
    const {getByText} = render(<ModalDelete setModalDeleteOpen={setModalDeleteOpenMock}/>);
    const cancelBtn = getByText('Cancel');

    cancelBtn.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(setModalDeleteOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalDeleteOpenMock).toHaveBeenCalledWith(false);
  });

  it('on click outside of modal window the setModalDeleteOpen is called', () => {
    const setModalDeleteOpenMock = jest.fn();
    render(<ModalDelete setModalDeleteOpen={setModalDeleteOpenMock}/>);
    const modal = document.querySelector('.modal');

    modal.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    expect(setModalDeleteOpenMock).toHaveBeenCalledTimes(1);
    expect(setModalDeleteOpenMock).toHaveBeenCalledWith(false);
  });
});