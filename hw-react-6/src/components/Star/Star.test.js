import React from 'react';
import {Star} from './Star';
import {render} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

describe('Icon star testing', () => {
  const testCard = {
    isFavorite: false
  };
  const testGoodsInfo = [
    {id: 1},
    {id: 2}
  ];
  const toggleFavoriteMock = jest.fn();

  it('icon star renders properly', () => {
    render(<Star card={testCard}/>);
    const star = document.querySelector('.fa-star');
    expect(star).toBeInTheDocument();
  });

  it('icon star has proper class before and after click', () => {

    toggleFavoriteMock.mockImplementation((card) => {
      card.isFavorite = !card.isFavorite
    });

    const {rerender} = render(<Star card={testCard}
                 isFavorite={testCard.isFavorite}
                 toggleFavorite={toggleFavoriteMock}
                 goodsInfo={testGoodsInfo}/>);
    const star = document.querySelector('.fa-star');

    expect(star).toHaveStyle('color: slategrey');

    star.dispatchEvent(new MouseEvent('click', {bubbles: true}));

    expect(toggleFavoriteMock).toHaveBeenCalledTimes(1);
    expect(toggleFavoriteMock).toHaveBeenCalledWith(testCard);
    expect(testCard.isFavorite).toBeTruthy();

    rerender(<Star card={testCard}
                   isFavorite={testCard.isFavorite}
                   toggleFavorite={toggleFavoriteMock}
                   goodsInfo={testGoodsInfo} />);

    expect(star).toHaveStyle('color: #d58e32');

    star.dispatchEvent(new MouseEvent('click', {bubbles: true}));
    rerender(<Star card={testCard}
                   isFavorite={testCard.isFavorite}
                   toggleFavorite={toggleFavoriteMock}
                   goodsInfo={testGoodsInfo} />);

    expect(star).toHaveStyle('color: slategrey');
    expect(testCard.isFavorite).toBeFalsy();
  });
});

