import React from 'react';
import {Cart} from './Cart';
import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

jest.mock('../../containers/Cards/Cards', () => {
  return (props) =>
    props.cardsData.map(card => {
        return (
          <div key={card.id}>
            {card.title}
          </div>
        )
      }
    )
});

jest.mock('../../components/EmptyState/EmptyState', () => {
  return (props) => (
    <div className="test-empty-state">
      No items in your cart
    </div>
  )
});

jest.mock("../../components/CartItem/CartItem");
jest.mock("../../components/Form/Form");

describe ('cart page testing', () => {

  const testCardsData = [
    {
      id: 1,
      title: 'testTitle',
      inCart: 1,
      price: 10
    },
    {
      id: 2,
      title: 'testTitle 2',
      inCart: 1,
      price: 20
    },
  ];
  const openFormMock = false;
  const setOpenFormMock = jest.fn();

  it('cart renders', () => {
    render(<Cart cardsData={testCardsData}
                 openForm={openFormMock}
                 setOpenForm={setOpenFormMock}/>)
  });

  it('empty state renders while cart is empty', () => {
    render(<Cart cardsData={[]}
                 openForm={openFormMock}
                 setOpenForm={setOpenFormMock}/>);
    const emptyStateDiv = document.querySelector('.test-empty-state');
    expect(emptyStateDiv).toBeInTheDocument();
  });

  it('total sum renders with correct value', () => {
    const {getByText} = render(<Cart cardsData={testCardsData}
                                     openForm={openFormMock}
                                     setOpenForm={setOpenFormMock}/>);
    expect(getByText(/total/i)).toBeInTheDocument();
    expect(getByText(/total/i).textContent).toBe('Total price: 30$');
  });

  it('on click to the button "buy now" the function ""setOpenForm is calling', () => {
    const {getByText} = render(<Cart cardsData={testCardsData}
                                     openForm={openFormMock}
                                     setOpenForm={setOpenFormMock}/>);
    const buyButton = getByText(/buy/i);
    expect(buyButton).toBeInTheDocument();
    fireEvent.click(buyButton);
    expect(setOpenFormMock).toHaveBeenCalledTimes(1);
    expect(setOpenFormMock).toHaveBeenCalledWith(true);
  });
});
