import React from 'react';
import {connect} from 'react-redux';

import './Favorites.scss';
import Cards from "../../containers/Cards/Cards";
import EmptyState from "../../components/EmptyState/EmptyState";
import Card from "../../components/Card/Card";

export const Favorites = (props) => {

  const {cardsData} = props;

  return (
    <div>
      {cardsData.length > 0 ? <Cards CardComponent={Card}
                                     cardsData={cardsData}
        />
        : <EmptyState text='No favorite items'/>}
    </div>
  )
};

const mapStateToProps = store => {
  return {
    cardsData: store.goodsInfo.filter(item => item.isFavorite)
  }
};

export default connect(mapStateToProps)(Favorites);