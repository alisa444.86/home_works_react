import React from 'react';
import {Favorites} from './Favorites';
import {render, fireEvent} from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

jest.mock('../../containers/Cards/Cards', () => {
  return (props) =>
    props.cardsData.map(card => {
        return (
          <div key={card.id}>
            {card.title}
          </div>
        )
      }
    )
});

jest.mock('../../components/EmptyState/EmptyState', () => {
  return (props) => (
    <div className="test-empty-state">
      No items in your cart
    </div>
  )
});

jest.mock("../../components/Card/Card");

describe('favorites page testing', () => {
  const testCardsData = [
    {
      id: 1,
      title: 'testTitle',
      inCart: 1,
      isFavorite: true
    },
    {
      id: 2,
      title: 'testTitle 2',
      inCart: 1,
      isFavorite: true
    },
  ];
  it('favorites page renders', () => {
    render(<Favorites cardsData={testCardsData}/>)
  });

  it('emptyState renders when there is not favorite item', () => {
    const {getByText} = render(<Favorites cardsData={[]}/>);
    expect(getByText(/no/i)).toBeInTheDocument();
  })
});