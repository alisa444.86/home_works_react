import React from 'react';
import {connect} from 'react-redux';

import Top from "../../components/Top/Top";
import Cards from "../../containers/Cards/Cards";
import Card from '../../components/Card/Card';

export const Home = (props) => {

  const {goodsInfo} = props;

  return (
    <div>
      <Top/>
      <Cards cardsData={goodsInfo}
             CardComponent={Card}
      />
    </div>
  )
};

const mapStateToProps = (store) => {
  return {
    goodsInfo: store.goodsInfo
  }
};

export default connect(mapStateToProps)(Home);