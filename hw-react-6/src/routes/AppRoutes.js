import React from 'react';
import {Route, Switch} from 'react-router-dom';

import Home from "../pages/Home/Home";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";
import './Approutes.scss';

function AppRoutes(props) {

  return (
    <div className='page-wrapper'>
      <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/cart' component={Cart}/>
        <Route exact path='/favorites' component={Favorites}/>
      </Switch>
    </div>
  );
}

export default AppRoutes;