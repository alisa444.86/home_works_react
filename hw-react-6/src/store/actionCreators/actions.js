import Actions from "../constants/constants";
import axios from "axios";
import {getStorageData, setStorageData} from "../../actions/storageActions";

export const setCurrentVendorCode = (vendorCode) => dispatch => {
  dispatch({type: Actions.SET_CURRENT_VENDORCODE, payload: vendorCode})
};

export const setModalAddOpen = (data) => dispatch => {
  dispatch({type: Actions.SET_MODAL_ADD_OPEN, payload: data})
};

export const setModalDeleteOpen = (data) => dispatch => {
  dispatch({type: Actions.SET_MODAL_DELETE_OPEN, payload: data})
};

export const increaseInCart = (item) => dispatch => {
  dispatch({type: Actions.INCREASE_AMOUNT_IN_CART, payload: item})
};

export const decreaseInCart = (item) => dispatch => {
  dispatch({type: Actions.DECREASE_AMOUNT_IN_CART, payload: item})
};

export const deleteFromCart = (item) => dispatch => {
  dispatch({type: Actions.DELETE_FROM_CART, payload: item})
};

export const toggleFavorite = (item) => dispatch => {
  dispatch({type: Actions.TOGGLE_FAVORITE, payload: item})
};

export const setOpenForm = (bool) => dispatch => {
  dispatch({type: Actions.OPEN_FORM, payload: bool})
};

export const clearStorage = (data) => dispatch => {
  const data = getStorageData();
  data.forEach(item => item.inCart = 0);
  setStorageData(data);
  dispatch({type: Actions.CLEAR_STORAGE, payload: data})
};

export const setFormData = (data) => dispatch => {
  dispatch({type: Actions.SUBMIT_FORM, payload: data})
};

export const getGoodsInfo = () => dispatch => {
  axios('/goods.json')
    .then(result => {
      const goodsInfo = result.data;
      const goodsInStorage = getStorageData();
      const info = [];

      goodsInfo.forEach(item => {
          const itemInStorage = goodsInStorage && goodsInStorage.find(el => el.vendorCode === item.vendorCode);
          if (itemInStorage) {
            item = itemInStorage;
          }
          info.push(item);
        }
      );
      dispatch({type: Actions.SET_GOODS_INFO, payload: info});
    })
};

export const logDeliveryData = (values) => dispatch => {
  const data = `Delivery Data: 
    name: ${values.name},
    last name: ${values.lastName},
    age: ${values.age},
    address: ${values.address},
    phone number: ${values.phone},
    email: ${values.email},
    delivery time: ${values.deliveryTime}`;
  dispatch({type: Actions.LOG_DELIVERY_DATA, payload: data})
};

export const logCheckoutData = (data) => dispatch => {
  const arrayData = [];
  data.forEach(item => {
    const str = ` ${item.title} - ${item.inCart} pc`;
    arrayData.push(str);
  });

  dispatch({type: Actions.LOG_CHECKOUT_DATA, payload: arrayData})
};

