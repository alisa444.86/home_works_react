import update from 'react-addons-update';
import Actions from '../constants/constants';

const goodsInfo = (state = [], action) => {
  let card = action.payload;
  let index = state.indexOf(card);
  let updatedCard;

  switch (action.type) {
    case Actions.SET_GOODS_INFO:
      return action.payload;
    case Actions.TOGGLE_FAVORITE:
      updatedCard = {...card, isFavorite: !card.isFavorite};
      return update(state, {$splice: [[index, 1, updatedCard]]});
    case Actions.INCREASE_AMOUNT_IN_CART:
      updatedCard = {...card, inCart: card.inCart += 1};
      return update(state, {$splice: [[index, 1, updatedCard]]});
    case Actions.DECREASE_AMOUNT_IN_CART:
      updatedCard = {...card, inCart: card.inCart -= 1};
      return update(state, {$splice: [[index, 1, updatedCard]]});
    case Actions.DELETE_FROM_CART:
      updatedCard = {...card, inCart: card.inCart = 0};
      return update(state, {$splice: [[index, 1, updatedCard]]});
    case Actions.GET_STORAGE_DATA:
      return action.payload;
    case Actions.SET_STORAGE_DATA:
      return action.payload;
    case Actions.CLEAR_STORAGE:
      return action.payload;
    default:
      return state;
  }
};

export default goodsInfo;