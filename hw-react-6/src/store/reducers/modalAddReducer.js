import Actions from '../constants/constants';

const modalAddOpen = (state = false, action) => {
  switch (action.type) {
    case Actions.SET_MODAL_ADD_OPEN:
      return action.payload;
    default:
      return state;
  }
};

export default modalAddOpen;