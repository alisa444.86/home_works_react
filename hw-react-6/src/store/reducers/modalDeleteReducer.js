import Actions from '../constants/constants';

const modalDeleteOpen = (state = false, action) => {
  switch (action.type) {
    case Actions.SET_MODAL_DELETE_OPEN:
      return action.payload;
    default:
      return state;
  }
};

export default modalDeleteOpen;