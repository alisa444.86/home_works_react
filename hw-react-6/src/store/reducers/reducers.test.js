import React from 'react';
import {rootReducer} from './rootReducer';
import '@testing-library/jest-dom/extend-expect';
import Actions from "../constants/constants";

it('reducer goodsInfo testing', () => {
  const state = rootReducer({
    goodsInfo: [],
    modalAddOpen: false,
    modalDeleteOpen: false,
    openForm: false,
    formData: {},
    logDeliveryData: {}
  }, {type: Actions.SET_GOODS_INFO, payload: [{id: 1}, {id: 2}]});

  expect(state.goodsInfo).toEqual([{id: 1}, {id: 2}]);
});


it('reducer setModalAddOpen testing', () => {
  const state = rootReducer({
    goodsInfo: [],
    modalAddOpen: false,
    modalDeleteOpen: false,
    openForm: false,
    formData: {},
    logDeliveryData: {}
    }, {type: Actions.SET_MODAL_ADD_OPEN, payload: true});

  expect(state.modalAddOpen).toBeTruthy();
});

it('reducer setModalDeleteOpen testing', () => {
  const state = rootReducer({
    goodsInfo: [],
    modalAddOpen: false,
    modalDeleteOpen: false,
    openForm: false,
    formData: {},
    logDeliveryData: {}
  }, {type: Actions.SET_MODAL_DELETE_OPEN, payload: true});

  expect(state.modalDeleteOpen).toBeTruthy();
});

it('reducer openForm testing', () => {
  const state = rootReducer({
    goodsInfo: [],
    modalAddOpen: false,
    modalDeleteOpen: false,
    openForm: false,
    formData: {},
    logDeliveryData: {}
  }, {type: Actions.OPEN_FORM, payload: true});

  expect(state.openForm).toBeTruthy();
});

it('reducer submitForm testing', () => {
  const testData = {
    name: 'testName',
    age: 20,
    lastName: 'testLastName'
  };

  const state = rootReducer({
    goodsInfo: [],
    modalAddOpen: false,
    modalDeleteOpen: false,
    openForm: false,
    formData: {},
    logDeliveryData: {}
  }, {type: Actions.SUBMIT_FORM, payload: testData});

  expect(state.formData).toEqual(testData);
});

it('reducer fromData testing', () => {
  const testData = {
    name: 'testName',
    age: 20,
    lastName: 'testLastName'
  };

  const state = rootReducer({
    goodsInfo: [],
    modalAddOpen: false,
    modalDeleteOpen: false,
    openForm: false,
    formData: {},
    logDeliveryData: {}
  }, {type: Actions.SUBMIT_FORM, payload: testData});

  expect(state.formData).toEqual(testData);
});
