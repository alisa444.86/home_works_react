import Actions from '../constants/constants';

const currentVendorCode = (state = 0, action) => {
  switch (action.type) {
    case Actions.SET_CURRENT_VENDORCODE:
      return action.payload;
    default:
      return state;
  }
};

export default currentVendorCode;